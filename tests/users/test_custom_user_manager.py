import pytest
from django.core.exceptions import ValidationError
from users.models import CustomUserManager 
from django.conf import settings


@pytest.mark.django_db
class TestCustomUserManagerCreateMethod:
    def test_email_present_and_condition_true(self):
        if settings.ENVIRONMENT != 'development' and settings.ENVIRONMENT != 'test':
            manager = CustomUserManager()
            user = manager.create(email="usuario@unb.br", password="teste123")
            assert user.email == "usuario@unb.br"  # Teste com credenciais validas

    def test_email_present_and_condition_false(self):
        if settings.ENVIRONMENT != 'development' and settings.ENVIRONMENT != 'test':
            manager = CustomUserManager()
            with pytest.raises(ValidationError):
                manager.create(email="usuario@unb.br")  # Teste com senha não fornecida

    def test_email_absent_and_condition_true(self):
        if settings.ENVIRONMENT != 'development' and settings.ENVIRONMENT != 'test':
            manager = CustomUserManager()
            with pytest.raises(ValueError):
                manager.create(email=None, password="teste123")  # Teste com email não fornecido

    def test_email_absent_and_condition_false(self):
        if settings.ENVIRONMENT != 'development' and settings.ENVIRONMENT != 'test':
            manager = CustomUserManager()
            with pytest.raises(ValueError):
                manager.create(email=None)  # Teste com email e senha não fornecidos





